#ifndef SENSORS_H
#define SENSORS_H

// GPS Libraries
#include <SoftwareSerial.h>
#include <TinyGPS.h>

// Accelerometer Libraries
#include <Adafruit_MPU6050.h>
#include <Wire.h>

class Sensors {
    public:
        float latitude, longitude;
        unsigned long age;
        sensors_event_t a, g, temp;
        int confirm, reset;

        Sensors(int gpsRx, int gpsTx) : gpsSerial(gpsRx, gpsTx) {}
        void init();

        bool update();
        int getSatellites();
    private:
        SoftwareSerial gpsSerial;
        TinyGPS gps;
        int confirmButton;
        int resetButton;

        bool updateGPS();
};

#endif