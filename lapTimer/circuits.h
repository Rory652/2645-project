#ifndef CIRCUITS_H
#define CIRCUITS_H

#include <SPI.h>
#include <SD.h>

const int maxPoints = 512;

// Struct for all the circuit data
typedef struct {
    float points[maxPoints][2];

    float maxLat, minLat, maxLon, minLon;

    int numPoints;
    int isCircuit;
} Circuit;

// Struct for timing laps
typedef struct {
    float startLat, startLon;
    bool finish;
    unsigned long startTime;
} Timer;

bool initSD();

// Functions for generating a circuit
void startCircuit(int isCircuit);
bool addPoint(float lat, float lon);
void finishCircuit();
int getNumCircuits();
void getCircuitName(char *buffer);

// Functions for loading a circuit
bool loadCircuit(String filename);
void readLine(File &file, String &line);
bool parseFloats(String &line, float &lat, float &lon);

// Functions for timing a circuit
void startLap(float startLat, float startLon);
int updatePosition(float lat, float lon);
float getRotation(float lat, float lon);
void rotateCoordinates(float &lat, float &lon, float angle);
int getNearestNode(float lat, float lon);
unsigned long getTime();
unsigned long getFinalTime(float lastLat, float lastLon, float currentLat, float currentLon, unsigned long lastTime);

Circuit& getCurrentCircuit();

#endif