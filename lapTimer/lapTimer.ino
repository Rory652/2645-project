#include "lcd.h"
#include "sensors.h"
#include "circuits.h"

// Different states of my program
// Doesn't include certain "mini-screens" such as the saving screen or lights screen
enum States {START, SELECT, GENERATE, READY, TIMING};
States state = START;
States previous = START;

Sensors *sensors;

// Flags to tell the program which button was last pressed
bool confirm = false;
bool reset = false;

// For button debouncing
unsigned long lastInterrupt = 0;
unsigned long debounceTime = 250;

// Type of circuit to generate
int circuitType = 1;

// Joystick pins
const int joyX = PC3;
const int joyY = PC2;

// Interrupt function for the confirm button
void confirmButton() {
    // Deal with button bouncing
    if (millis() - lastInterrupt <= debounceTime) {
        return;
    }
    lastInterrupt = millis();

    confirm = true;
    reset = false;

    previous = state;

    // Treat as state machine (transition is "confirm")
    if (state == START) {
        state = SELECT;
    } else if (state == SELECT) {
        state = READY;
    } else if (state == GENERATE) {
        state = READY;
    } else if (state == READY) {
        state = TIMING;
    }
}

// Interrupt function for the confirm button
void resetButton() {
    // Deal with button bouncing
    if (millis() - lastInterrupt <= debounceTime) {
        return;
    }
    lastInterrupt = millis();

    reset = true;
    confirm = false;

    previous = state;

    // Treat as state machine (transition is "reset")
    if (state == START) {
        state = GENERATE;
    } else if (state == SELECT) {
        state = START;
    } else if (state == GENERATE) {
        state = START;
    } else if (state == READY) {
        state = START;
    } else if (state == TIMING) {
        state = READY;
    }
}

bool checkSD() {
    // Check SD card:
    File test = SD.open("test.txt", FILE_WRITE);

    // Problem with SD card if file cannot be opened (it will create it if the the file doesn't exist)
    if (!test) {
        drawError("SD Card\nnot found");

        initSD();
        return false;
    } else {
        test.close();
    }

    return true;
}

bool checkGPS() {
    // Check GPS:
    if (sensors->latitude == TinyGPS::GPS_INVALID_F_ANGLE || sensors->longitude == TinyGPS::GPS_INVALID_F_ANGLE) {
        drawError("GPS not\nworking");
        return false;
    }

    // Invalid value is 255 which looks like its working when it isn't
    int satellites = sensors->getSatellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : sensors->getSatellites();
    if (satellites == 0) {
        drawError("GPS can't\nfind any\nsatellites\n");

        return false;
    }

    return true;
}

bool checkComponents() {
    bool newData = sensors->update();

    if (!newData) {
        drawError("GPS not\nupdating");
        return false;
    }

    return checkSD() && checkGPS();
}

void errorFound() {
    state = START;

    // Resets the display and checks until SD and GPS are working correctly
    while (!checkComponents()) {
        delay(10000);
    }

    drawStart(circuitType);
}

void setup() {
    initLCD();
    
    if (!initSD()) {
        drawError("SD Card\ncould not\nstart");
        delay(10000);
    }

    // Create Sensors object with GPS pins
    sensors = new Sensors(PC0, PC1);
    sensors->init();

    // Check that every component is working correctly and check again every 10 seconds if it isn't
    while (!checkComponents()) {
        delay(10000);
    }

    // Setup confirm button
    pinMode(PC10, INPUT);
    attachInterrupt(digitalPinToInterrupt(PC10), confirmButton, RISING);

    // Setup reset button
    pinMode(PC12, INPUT);
    attachInterrupt(digitalPinToInterrupt(PC12), resetButton, RISING);

    // Setup Joystick
    pinMode(joyX, INPUT);
    pinMode(joyY, INPUT);

    drawStart(circuitType);
}

void loop() {
    // Acting on changes in interrupt functions
    if (state == START) {
        if (confirm) {
            confirm = false;

            drawStart(circuitType);
        } else if (reset) {
            reset = false;

            drawStart(circuitType);
        }

        if (analogRead(joyX) >= 750 || analogRead(joyY) >= 750) {
            // Increment the circuit type
            circuitType = (circuitType + 1) % 2;
            drawStart(circuitType);

            delay(1000);
        } else if (analogRead(joyX) <= 250 || analogRead(joyY) <= 250) {
            // Decrement the circuit type
            circuitType = (circuitType - 1) % 2;
            drawStart(circuitType);

            delay(1000);
        }
    } else if (state == SELECT) {
        static int selected = 0;
        static int numCircuits = getNumCircuits();

        bool valid;
        bool changed = false;
        char buffer[64];

        if (confirm) {
            confirm = false;

            selected = 0;
            numCircuits = getNumCircuits();

            if (numCircuits == 0) {
                drawError("No files found");
                delay(10000);
                state = START;
                drawStart(circuitType);

                // Restart Loop
                return;
            } else {
                changed = true;
            }
        } else if (reset) {
            reset = false;

            selected = 0;
            numCircuits = getNumCircuits();
        }

        if (analogRead(joyX) >= 750 || analogRead(joyY) >= 750) {
            selected++;
            if (selected == numCircuits) {
                selected = 0;
            }

            changed = true;
        } else if (analogRead(joyX) <= 250 || analogRead(joyY) <= 250) {
            selected--;
            if (selected == -1) {
                selected = numCircuits - 1;
            }

            changed = true;
        }

        if (changed) {
            sprintf(buffer, "c%i.txt", selected);

            valid = loadCircuit(buffer);
            if (!valid) {
                drawError("Invalid\nFile");
                delay(10000);
                
                state = START;
                drawStart(circuitType);

                return;
            }

            drawSelect(selected);
            drawCircuit(getCurrentCircuit());
        }
    } else if (state == GENERATE) {
        if (confirm) {
            confirm = false;
        } else if (reset) {
            reset = false;

            // Circuit generation sequence
            drawGenerate();
            startCircuit(circuitType);
            drawCircuit(getCurrentCircuit());
        }

        // Update GPS values
        bool newData = sensors->update();

        // Check GPS is still working correctly
        if (!checkGPS() || !newData) {
            errorFound();

            return;
        }

        // Add point to circuit
        bool pointAdded = addPoint(sensors->latitude, sensors->longitude);

        // Check if circuit exceeds maximum number of points
        if (pointAdded) {
            drawCircuit(getCurrentCircuit());
        } else {
            state == START;
            // Display and error and return to the start
            drawError("Circuit is Too Large");
            delay(10000);
            drawStart(circuitType);

            return;
        }
    } else if (state == READY) {
        if (confirm) {
            confirm = false;

            // Different actions if previous state was select or generate (one has to load the circuit in)
            if (previous == SELECT) {
                // Circuit already loaded in the SELECT section
            } else if (previous == GENERATE) {
                drawSaving();

                // Check that SD card works
                if (!checkSD()) {
                    errorFound();
                    
                    return;
                }

                finishCircuit();
            }

            drawReady(0);
            drawCircuit(getCurrentCircuit());
        } else if (reset) {
            reset = false;

            drawReady(0);
            drawCircuit(getCurrentCircuit());
        }
    } else if (state == TIMING) {
        if (confirm) {
            confirm = false;

            // To get start position
            sensors->update();
            drawLights();
            startLap(sensors->latitude, sensors->longitude);

            // To let lights stay on the display for a second
            delay(1000);

            drawTimer();
            updateTimer(getTime());
            drawCircuit(getCurrentCircuit());
        } else if (reset) {
            reset = false;
        }

        bool newData = sensors->update();

        // Check GPS is still working correctly
        if (!checkGPS() || !newData) {
            errorFound();
        }

        // Static variables to hold information about previous reading
        static int lastNode = -1;
        static float lastLat, lastLon;
        static unsigned long lastTime = 0;

        // Get the current node based on GPS position
        int currentNode = updatePosition(sensors->latitude, sensors->longitude);

        // Display new position
        showPosition(getCurrentCircuit(), lastNode, currentNode);

        if (currentNode >= 0) {
            lastTime = getTime();
            updateTimer(lastTime);
        } else {
            // Lap finished
            state = READY;
            drawReady(getFinalTime(lastLat, lastLon, sensors->latitude, sensors->longitude, lastTime));
            drawCircuit(getCurrentCircuit());
        }

        lastNode = currentNode;
        lastLat = sensors->latitude;
        lastLon = sensors->longitude;
    }

    delay(50);
}