#include "sensors.h"

// How many milliseconds to read data from the GPS
const unsigned long gpsReadTime = 1000;

void Sensors::init() {
    // Start serial for GPS
    gpsSerial.begin(9600);
}
        
bool Sensors::update() {
    return updateGPS();
}

bool Sensors::updateGPS() {
    bool newData = false;
    // For one second we parse GPS data and report some key values
    for (unsigned long start = millis(); millis() - start < gpsReadTime;) {
        while (gpsSerial.available()) {
            char c = gpsSerial.read();
            if (gps.encode(c)) {
                newData = true;
            }
        }
    }

    if (newData) {
        gps.f_get_position(&latitude, &longitude, &age);
    }

    return newData;
}

int Sensors::getSatellites() {
    return gps.satellites();
}