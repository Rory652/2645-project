#ifndef LCD_H
#define LCD_H

#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>

#include "circuits.h"

// Define display pins
#define TFT_CS        D10
#define TFT_RST       D9
#define TFT_DC        D8

void initLCD();

void drawStart(int type);
void drawError(String err);
void drawSelect(int circuitNumber);
void drawGenerate();
void drawSaving();
void drawReady(unsigned long finalTime);
void drawLights();
void drawTimer();
void updateTimer(unsigned long time);

void adjustRanges(float latRange, float lonRange, int &latOffset, int &lonOffset);
void drawCircuit(Circuit &circuit);
void showPosition(Circuit &circuit, int last, int current);

void drawCenter(String str, int y);

#endif