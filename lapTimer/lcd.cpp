#include "lcd.h"

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

const int numScreens = 5;
int screen = 0;

void initLCD() {
    tft.initR(INITR_BLACKTAB);
    tft.fillScreen(ST77XX_BLACK);
}

void drawStart(int type) {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(4);
    drawCenter("Lap", 5);
    drawCenter("Timer", 42);

    tft.setTextSize(1);
    tft.setCursor(1, 134);

    tft.println("'confirm' to load");

    if (type == 0) {
        tft.println("'reset' to generate a sprint");
    } else {
        tft.println("'reset' to generate a circuit");
    }
}

void drawError(String err) {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(4);
    drawCenter("Error", 5);

    tft.setTextSize(2);
    tft.setCursor(5, 42);

    tft.println(err);
}

void drawSelect(int circuitNumber) {
    char buffer[64];
    sprintf(buffer, "Select\nCircuit %i", circuitNumber);

    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(2);
    drawCenter(buffer, 5);

    tft.setTextSize(1);
    tft.setCursor(1, 142);
    tft.println("'confirm' to select");
    tft.println("'reset' to cancel");
}

void drawGenerate() {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(2);
    drawCenter("Generating Circuit", 5);

    tft.setTextSize(1);
    tft.setCursor(1, 142);
    tft.println("'confirm' to finish");
    tft.println("'reset' to cancel");
}

void drawSaving() {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(2);
    drawCenter("Saving Circuit", 5);
}

void drawReady(unsigned long finalTime) {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(4);
    drawCenter("Ready", 5);

    if (finalTime > 0) {
        // If a lap is finished, show the final time
        updateTimer(finalTime);
    }

    tft.setTextSize(1);
    tft.setCursor(1, 142);
    tft.println("'confirm' to start");
    tft.println("'reset' to cancel");
}

void drawLights() {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    // Row 1
    for (int16_t i = 0; i < 5; i++) {
        tft.fillCircle(80, 20+28*i+3*(i-1), 14, 0x9492);
        tft.drawCircle(80, 20+28*i+3*(i-1), 14, ST77XX_WHITE);
    }

    // Row 2 (Goes green when lap starts)
    for (int16_t i = 0; i < 5; i++) {
        tft.fillCircle(40, 20+28*i+3*(i-1), 14, 0x9492);
        tft.drawCircle(40, 20+28*i+3*(i-1), 14, ST77XX_WHITE);
    }

    delay(2000);

    // Do Red Lights
    for (int16_t i = 0; i < 5; i++) {
        tft.fillCircle(80, 20+28*i+3*(i-1), 14, ST77XX_RED);
        tft.drawCircle(80, 20+28*i+3*(i-1), 14, ST77XX_WHITE);
        delay(500);
    }

    // Do Green Lights
    for (int16_t i = 0; i < 5; i++) {
        tft.fillCircle(40, 20+28*i+3*(i-1), 14, ST77XX_GREEN);
        tft.drawCircle(40, 20+28*i+3*(i-1), 14, ST77XX_WHITE);
    }
}

void drawTimer() {
    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    tft.setTextSize(4);
    drawCenter("Time:", 5);

    tft.setTextSize(1);
    tft.setCursor(1, 150);
    tft.println("'reset' to cancel");
}

void updateTimer(unsigned long time) {
    int minutes = time / (60 * 1000);
    int seconds = (time / 1000) % 60;
    int ms = time % 1000;

    char timeStr[64];
    sprintf(timeStr, "%i:%02i.%03i", minutes, seconds, ms);

    // Clear previous time
    tft.fillRect(0, 42, 128, 20, ST77XX_BLACK);

    tft.setTextSize(2);
    drawCenter(timeStr, 42);
}

void adjustRanges(float latRange, float lonRange, int &latOffset, int &lonOffset) {
    float latPerPixel = latRange / 50.0;
    float lonPerPixel = lonRange / 100.0;

    float largest = latPerPixel > lonPerPixel ? latPerPixel : lonPerPixel;

    latOffset = (50 - round(latRange / largest)) / 2;
    lonOffset = (100 - round(lonRange / largest)) / 2;
}

void drawCircuit(Circuit &circuit) {
    tft.fillRect(9, 70, 110, 60, ST77XX_BLACK);
    tft.drawRect(9, 70, 110, 60, ST77XX_WHITE);

    // Area the circuit itself is drawn in (not where the border rectangle is)
    static const int circuitArea[2][2] = {{14, 75}, {114, 125}};

    switch (circuit.numPoints) {
        case 0:
            break;
        case 1:
            tft.fillCircle((circuitArea[0][0] + circuitArea[1][0])/2, (circuitArea[0][1] + circuitArea[1][1])/2, 2, ST77XX_WHITE);
            break;
        default:
            float latRange = circuit.minLat - circuit.maxLat, lonRange = circuit.maxLon - circuit.minLon;
            int latOffset, lonOffset;
            adjustRanges(-latRange, lonRange, latOffset, lonOffset);
            
            float lcdLatRange = 50.0 - (latOffset * 2);
            float lcdLonRange = 100.0 - (lonOffset * 2);

            int lastLat, lastLon;

            for (int i = 0; i < circuit.numPoints; i++) {
                int latPos = circuitArea[0][1] + round((lcdLatRange/latRange)*(circuit.points[i][0] - circuit.maxLat)) + latOffset;
                int lonPos = circuitArea[0][0] + round((lcdLonRange/lonRange)*(circuit.points[i][1] - circuit.minLon)) + lonOffset;

                tft.fillCircle(lonPos, latPos, 2, ST77XX_WHITE);

                if (i > 0) {
                    tft.drawLine(lastLon, lastLat, lonPos, latPos, ST77XX_WHITE);
                } else {
                    // Colour start node green
                    tft.fillCircle(lonPos, latPos, 2, ST77XX_GREEN);
                }

                lastLat = latPos;
                lastLon = lonPos;
            }

            // Draw line for final node to start/finish line if it's a circuit
            if (circuit.isCircuit == 1) {
                int startLat = circuitArea[0][1] + round((lcdLatRange/latRange)*(circuit.points[0][0] - circuit.maxLat)) + latOffset;
                int startLon = circuitArea[0][0] + round((lcdLonRange/lonRange)*(circuit.points[0][1] - circuit.minLon)) + lonOffset;


                tft.drawLine(lastLon, lastLat, startLon, startLat, ST77XX_WHITE);
            } else {
                // Colour finish node red
                tft.fillCircle(lastLon, lastLat, 2, ST77XX_RED);
            }
    }
}

void showPosition(Circuit &circuit, int last, int current) {
    // Area the circuit itself is drawn in (not where the border rectangle is)
    static const int circuitArea[2][2] = {{14, 75}, {114, 125}};

    int latPos, lonPos;

    float latRange = circuit.minLat - circuit.maxLat, lonRange = circuit.maxLon - circuit.minLon;
    int latOffset, lonOffset;
    adjustRanges(-latRange, lonRange, latOffset, lonOffset);

    float lcdLatRange = 50 - (latOffset * 2);
    float lcdLonRange = 100 - (lonOffset * 2);

    // Turn last position back to white
    if (last != -1 && last != 0) {
        latPos = circuitArea[0][1] + round((lcdLatRange/latRange)*(circuit.points[last][0] - circuit.maxLat)) + latOffset;
        lonPos = circuitArea[0][0] + round((lcdLonRange/lonRange)*(circuit.points[last][1] - circuit.minLon)) + lonOffset;


        tft.fillCircle(lonPos, latPos, 2, ST77XX_WHITE);
    }

    // Turn current position to blue
    if (current != 0) {
        latPos = circuitArea[0][1] + round((lcdLatRange/latRange)*(circuit.points[current][0] - circuit.maxLat)) + latOffset;
        lonPos = circuitArea[0][0] + round((lcdLonRange/lonRange)*(circuit.points[current][1] - circuit.minLon)) + lonOffset;

        tft.fillCircle(lonPos, latPos, 2, ST77XX_BLUE);
    }
}

void drawCenter(String str, int y) {
    int16_t x1, y1;
    uint16_t w, h;

    // Get string width
    tft.getTextBounds(str, 0, y, &x1, &y1, &w, &h);
    // Position Cursor
    tft.setCursor(64 - (w/2), y);
    tft.print(str);
}