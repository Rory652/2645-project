#include "circuits.h"

const int sdCS = PB10;

Circuit circuitData;
Timer circuitTimer;

bool initSD() {
    return SD.begin(sdCS);
}

void startCircuit(int isCircuit) {
    circuitData.isCircuit = isCircuit;
    circuitData.numPoints = 0;

    // Starting bounds can't be zero of there's a chance of dividing by zero
    circuitData.maxLat =  0.0000001; 
    circuitData.minLat = -0.0000001; 
    circuitData.maxLon =  0.0000001; 
    circuitData.minLon = -0.0000001;
}

bool addPoint(float lat, float lon) {
    static float latStart, lonStart;

    // Set start latitude and longitude to find relative positions
    if (circuitData.numPoints == 0) {
        latStart = lat;
        lonStart = lon;
    }

    circuitData.points[circuitData.numPoints][0] = lat - latStart;
    circuitData.points[circuitData.numPoints][1] = lon - lonStart;

    // Update bounds
    if (circuitData.points[circuitData.numPoints][0] > circuitData.maxLat) {
        circuitData.maxLat = circuitData.points[circuitData.numPoints][0];
    }
    if (circuitData.points[circuitData.numPoints][0] < circuitData.minLat) {
        circuitData.minLat = circuitData.points[circuitData.numPoints][0];
    }

    if (circuitData.points[circuitData.numPoints][1] > circuitData.maxLon) {
        circuitData.maxLon = circuitData.points[circuitData.numPoints][1];
    }
    if (circuitData.points[circuitData.numPoints][1] < circuitData.minLon) {
        circuitData.minLon = circuitData.points[circuitData.numPoints][1];
    }

    circuitData.numPoints++;

    // Check if circuit is too big
    if (circuitData.numPoints > maxPoints) {
        return false;
    } else {
        return true;
    }
}

void finishCircuit() {
    char filename[32], buffer[128], latStr[16], lonStr[16];

    getCircuitName(filename);

    Serial.println(filename);

    File file = SD.open(filename, FILE_WRITE);

    // Save upper bounds
    dtostrf(circuitData.maxLat, 12, 9, latStr);
    dtostrf(circuitData.maxLon, 12, 9, lonStr);
    sprintf(buffer, "%s, %s", latStr, lonStr);

    file.println(buffer);

    // Save lower bounds (and circuit type)
    dtostrf(circuitData.minLat, 12, 9, latStr);
    dtostrf(circuitData.minLon, 12, 9, lonStr);
    sprintf(buffer, "%s, %s\n%i", latStr, lonStr, circuitData.isCircuit);

    file.println(buffer);

    for (int i = 0; i < circuitData.numPoints; i++) {
        // Written "latitude, longitude" - relative to start point
        dtostrf(circuitData.points[i][0], 12, 9, latStr);
        dtostrf(circuitData.points[i][1], 12, 9, lonStr);
        
        sprintf(buffer, "%s, %s", latStr, lonStr);

        file.println(buffer);
    }

    file.close();
}

// Finds how many circuits are saved on the SD card
int getNumCircuits() {
    int toTry = 0;
    char buffer[64];
    sprintf(buffer, "c%i.txt", toTry);

    while (SD.exists(buffer)) {
        toTry++;
        sprintf(buffer, "c%i.txt", toTry);
    }

    return toTry;
}

// Gets file name for the new circuit
void getCircuitName(char *buffer) {
    sprintf(buffer, "c%i.txt", getNumCircuits());
}

bool loadCircuit(String filename) {
    File file = SD.open(filename, FILE_READ);

    if (file) {
        String line;
        float lat, lon;
        bool validLine;

        circuitData.numPoints = 0;

        // Can assume file has at least 4 lines (otherwise automatically invalid)
        // Read in max bounds
        readLine(file, line);
        validLine = parseFloats(line, lat, lon);

        if (validLine) {
            circuitData.maxLat = lat; 
            circuitData.maxLon = lon; 
        } else {
            file.close();
            return false;
        }

        // Read in min bounds
        readLine(file, line);
        validLine = parseFloats(line, lat, lon);

        if (validLine) {
            circuitData.minLat = lat; 
            circuitData.minLon = lon; 
        } else {
            file.close();
            return false;
        }

        // Read in circuit type
        readLine(file, line);
        // If something is invalid here it will default to a 0 (circuit with seperate start and finish)
        circuitData.isCircuit = line.toInt();

        while (file.available()) {
            readLine(file, line);
            validLine = parseFloats(line, lat, lon);

            if (validLine) {
                circuitData.points[circuitData.numPoints][0] = lat; 
                circuitData.points[circuitData.numPoints][1] = lon;

                circuitData.numPoints++;
            } else {
                file.close();
                return false;
            }
        }

    } else {
        return false;
    }

    file.close();
    return true;
}

void readLine(File &file, String &line) {
    line = file.readStringUntil('\n');

    // Remove a trailing carriage return if necessary
    line.replace("\r", "");
    line.replace(" ", "");
}

bool parseFloats(String &line, float &lat, float &lon) {
    // Find where the comma is
    int commaPos = line.indexOf(',');

    // Read in floats either side of the comma
    if (commaPos != -1) {
        lat = line.substring(0, commaPos).toFloat();
        lon = line.substring(commaPos +1).toFloat();
    } else {
        return false;
    }

    return true;
}

void startLap(float startLat, float startLon) {
    circuitTimer.startLat = startLat;
    circuitTimer.startLon = startLon;

    circuitTimer.startTime = millis();

    circuitTimer.finish = false;
}

// Update users position and return that node to be displayed - returns -1 if lap is finished
int updatePosition(float lat, float lon) {
    // Stores rotation
    static float rotation = -1;
    // Threshold for number of nodes you have to be within start/finish for lap to count
    const int threshold = 5;

    float relLat = lat - circuitTimer.startLat;
    float relLon = lon - circuitTimer.startLon;

    if (rotation == -1 && (lat != circuitTimer.startLat || lon != circuitTimer.startLon)) {
        rotation = getRotation(relLat, relLon);
    }

    rotateCoordinates(relLat, relLon, rotation);

    int nearestNode = getNearestNode(relLat, relLon);

    // Checks for circuits
    if (circuitData.isCircuit == 1) {
        // Check if almost at finish line
        if (!circuitTimer.finish && nearestNode >= circuitData.numPoints - threshold) {
            circuitTimer.finish = true;
        }

        // Check if past finish line (lap finished)
        if (circuitTimer.finish && nearestNode <= threshold) {
            return -1;
        }
    } else {
        // Hill climb type circuits
        if (!circuitTimer.finish && nearestNode > circuitData.numPoints - threshold) {
            circuitTimer.finish = true;
        }

        // Check if past finish line (lap finished)
        if (circuitTimer.finish && (nearestNode < circuitData.numPoints - threshold || nearestNode == (circuitData.numPoints - 1))) {
            return -1;
        }
    }

    return nearestNode;
}

// Gets rotation of circuit based on initial direction - returns value in radians
float getRotation(float lat, float lon) {
    // Don't rotate if below a certain threshold
    static const float angleThreshold = M_PI / 18;

    float dotProduct = (lat * circuitData.points[1][0]) + (lon * circuitData.points[1][1]);
    
    float pointLength = sqrt(sq(lat) + sq(lon));
    float circuitLength = sqrt(sq(circuitData.points[1][0]) + sq(circuitData.points[1][1]));

    float angle = acos(dotProduct / (pointLength * circuitLength));

    return abs(angle) > angleThreshold ? angle : 0.0;
}

// Rotates a set of coordinates about origin by rotation given (in radians)
void rotateCoordinates(float &lat, float &lon, float angle) {
    float newLat = lon * sin(angle) + lat * cos(angle);
    float newLon = lon * cos(angle) - lat * sin(angle);

    lat = newLat;
    lon = newLon;
}

int getNearestNode(float lat, float lon) {
    int nearest = 0;
    float minDistance = 10000.0;

    // Check every single point and find the distance between that and the current position
    for (int i = 0; i < circuitData.numPoints; i++) {
        float distance = sqrt(sq(lat - circuitData.points[i][0]) + sq(lon - circuitData.points[i][1]));

        if (distance < minDistance) {
            minDistance = distance;
            nearest = i;
        }
    }

    // Return index of the nearest node
    return nearest;
}

unsigned long getTime() {
    return millis() - circuitTimer.startTime;
}

// Gets final time as accurately as possible by using the Haversine formula on the previous and current coordinates
unsigned long getFinalTime(float lastLat, float lastLon, float currentLat, float currentLon, unsigned long lastTime) {
    // Constant values for the formula
    static const float earthRadius = 6371.0;
    static const float degToRadians = 0.017453292519943295;

    unsigned long currentTime = getTime();

    // Calculates distance between the last lat/lon and the current lat/lon in meters
    float distance = 2 * earthRadius * 1000 * asin(sqrt(0.5 - cos((currentLat - lastLat) * degToRadians)/2 + cos(lastLat * degToRadians) * cos(currentLat * degToRadians) * (1 - cos((currentLon - lastLon) * degToRadians))/2));
    float speed = distance / ((float) currentTime - (float) lastTime);

    // Calculates distance from finish line to current lat/lon
    float distanceToFinish = 0;
    if (circuitData.isCircuit == 1) {
        distanceToFinish = 2 * earthRadius * 1000 * asin(sqrt(0.5 - cos(currentLat * degToRadians)/2 + cos(currentLat * degToRadians) * (1 - cos(currentLon * degToRadians))/2));
    } else {
        float finishLat = circuitData.points[circuitData.numPoints-1][0];
        float finishLon = circuitData.points[circuitData.numPoints-1][1];

        distanceToFinish = 2 * earthRadius * 1000 * asin(sqrt(0.5 - cos((currentLat - finishLat) * degToRadians)/2 + cos(finishLat * degToRadians) * cos(currentLat * degToRadians) * (1 - cos((currentLon - finishLon) * degToRadians))/2));
    }
    
    unsigned long timeToFinish = distanceToFinish / speed;

    return currentTime - timeToFinish;
}

Circuit& getCurrentCircuit() {
    return circuitData;
}
