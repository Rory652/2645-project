// Display Libraries
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include <SPI.h>

// GPS Libraries
#include <SoftwareSerial.h>
#include <TinyGPS.h>

// Accelerometer Libraries
#include <Adafruit_MPU6050.h>
#include <Wire.h>

#include <SD.h>

// Define and initialise display
#define TFT_CS        D10
#define TFT_RST       D9
#define TFT_DC        D8

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);

// Intialise GPS
TinyGPS gps;
SoftwareSerial gpsSerial(PC0, PC1);

const int confirmButton = PC10;
const int resetButton = PC12;

const int joyX = PC3;
const int joyY = PC2;

const int sdCS = PB10;
Sd2Card card;

Adafruit_MPU6050 mpu;

void setup(void) {
    gpsSerial.begin(9600);

    mpu.begin();
    mpu.setAccelerometerRange(MPU6050_RANGE_2_G);
    mpu.setGyroRange(MPU6050_RANGE_250_DEG);
    mpu.setFilterBandwidth(MPU6050_BAND_94_HZ);

    pinMode(confirmButton, INPUT);
    pinMode(resetButton, INPUT);

    // Initalise screen and clear it
    tft.initR(INITR_BLACKTAB);
    tft.fillScreen(ST77XX_BLACK);

    delay(1000);
}

void loop() {
    float flat, flon;
    unsigned long age;
    bool newData = false;

    sensors_event_t a, g, temp;

    // For one second we parse GPS data and report some key values
    for (unsigned long start = millis(); millis() - start < 1000;) {
        while (gpsSerial.available()) {
            char c = gpsSerial.read();
            if (gps.encode(c)) {
                newData = true;
            }
        }
    }

    // Update sensore values
    gps.f_get_position(&flat, &flon, &age);
    mpu.getEvent(&a, &g, &temp);

    // Clear Screen
    tft.fillScreen(ST77XX_BLACK);

    tft.setCursor(0, 0);
    tft.setTextColor(ST77XX_WHITE);
    tft.setTextWrap(true);

    // GPS Data
    tft.print("Lat: ");
    tft.println(flat, 6);
    tft.print("Lon: ");
    tft.println(flon, 6);

    tft.println();

    // Accelerometer Data
    tft.print("Acceleration X: ");
    tft.println(a.acceleration.x);
    tft.print("Rotation X: ");
    tft.println(g.gyro.x);
    tft.print("Acceleration Y: ");
    tft.println(a.acceleration.y);
    tft.print("Rotation Y: ");
    tft.println(g.gyro.y);
    tft.print("Acceleration Z: ");
    tft.println(a.acceleration.z);
    tft.print("Rotation Z: ");
    tft.println(g.gyro.z);

    tft.println();

    // Buttons
    tft.print("Confirm Button: ");
    tft.println(digitalRead(confirmButton));
    tft.print("Reset Button: ");
    tft.println(digitalRead(resetButton));

    tft.println();

    // Joystick
    tft.print("Joystick X: ");
    tft.println(analogRead(joyX));
    tft.print("Joystick Y: ");
    tft.println(analogRead(joyY));

    tft.println();

    // SD Card
    if (card.init(SPI_HALF_SPEED, sdCS)) {
        tft.println("SD Card Read Okay");
    } else {
        tft.println("SD Card Not Read");
    }

    delay(1000);
}