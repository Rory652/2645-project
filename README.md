# 2645 Lap Timer Project

Lap Timer project for ELEC2645 Coursework. Project allows the user to generate a circuit using GPS data and then time themselves around it. Circuits can be timed anywhere as the GPS data is not specific to a single location.

## Parts List
- [Nucleo 64 L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html)
- [Adafruit 1.8" TFT with SD Card](https://learn.adafruit.com/1-8-tft-display)
- [GY-NEO6MV2 NEO-6M GPS](https://www.amazon.co.uk/gp/product/B088LR3488/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)
- [Generic Joystick](https://www.amazon.co.uk/gp/product/B06WRRKS9G/ref=ppx_yo_dt_b_asin_title_o02_s01?ie=UTF8&psc=1)
- 2x Generic Buttons
- Generic Red LED
- Generic Green LED
- Generic Micro SD Card formatted to FAT32

## Wiring
![Wiring Diagram](diagram.jpg)

## How to Run

### Setting Up Arduino IDE
1. Download the Arduino IDE from [here](https://www.arduino.cc/en/software)
2. Open `file->prefences` and copy this link into the Additional Board Managers field: `https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json`
3. Open `Tools->Board->Board Manager` and search for `STM32 MCU Based Boards` and install the first option
4. Open `Tools->Board` and change the board to `Nucleo 64`
5. Open `Tools->Board Part Number` and select `Nucleo L476RG`

### Running the Program
1. Clone the repository using `git clone https://gitlab.com/Rory652/2645-project.git`
2. Copy the folders in the `Libraries` folder into the Arduino Libraries folder. This will either be where you installed the program or in Documents
3. Open `lapTimer.ino` in the IDE
4. Change the COM port if necessary, compile the program and upload
